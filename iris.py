import time
import numpy as np

# https://machinelearningmastery.com/how-to-fix-futurewarning-messages-in-scikit-learn/
# import warnings filter
from warnings import simplefilter
# ignore all "future warnings"
simplefilter(action='ignore', category=FutureWarning)

# dataset
from sklearn.datasets import load_iris

# testing and tuning hyperparameters
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV

# models
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.neural_network import MLPClassifier
from sklearn import svm  # TODO: import this as SVC?
from sklearn.neighbors import KNeighborsClassifier

# data exploration
from yellowbrick.target import ClassBalance
from yellowbrick.target import FeatureCorrelation

# model selection and tuning hyper parameters
from yellowbrick.model_selection import LearningCurve
from yellowbrick.model_selection import ValidationCurve
from yellowbrick.classifier import ClassPredictionError


# global variables
random_state = 55
dataset_name = 'iris'
feature_names = ['sepal length (cm)', 'sepal width (cm)', 'petal length (cm)', 'petal width (cm)']
target_names = ['setosa', 'versicolor', 'virginica']

# using iris dataset
# print(load_iris())
X, y = load_iris(return_X_y=True)

# split dataset for classification problem (stratify tries to get equal number of each target class)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, stratify=y, random_state=random_state)
# print(y_train)

# number of features
# print(len(X[0]))


def get_chart_outpath(chart_name):
  return f'charts/{dataset_name}/{chart_name}.png'


def class_balance(y):
  print('running class_balance')

  # Instantiate the visualizer
  visualizer = ClassBalance(labels=target_names)

  visualizer.fit(y)        # Fit the data to the visualizer

  outpath = get_chart_outpath(chart_name='class-balance')
  visualizer.show(outpath=outpath)        # Finalize and render the figure
  visualizer.poof(clear_figure=True)  # reset plot


def feature_correlation(X, y):
  print('running feature_correlation')

  # Instantiate the visualizer
  visualizer = FeatureCorrelation(labels=feature_names, size=(1000, 550))  # make plot wider than the default 800x550 pixels

  visualizer.fit(X, y)        # Fit the data to the visualizer

  outpath = get_chart_outpath(chart_name='feature-correlation')
  visualizer.show(outpath=outpath)        # Finalize and render the figure
  visualizer.poof(clear_figure=True)  # reset plot


def learning_curve(X, y, model, chart_name=''):
  '''
  This method generates a Learning Curve chart for a given model.
  It runs in ~0.9 seconds for Decision Tree
    Finished in [0.8959] seconds
    Finished in [0.9006] seconds
  '''
  print(f'running learning_curve [{chart_name}]')
  

  cv = 5  # cross validation folds
  sizes = np.linspace(0.1, 1.0, 5)
  # sizes = np.linspace(0.1, 1.0, 10)
  # sizes = [30, 60, 90, 120]

  visualizer = LearningCurve(
    model,
    cv=cv,
    scoring='f1_weighted',
    train_sizes=sizes,
    n_jobs=5,
    random_state=random_state
  )

  # start time
  start = time.time()

  # Fit the training data to the visualizer
  # visualizer.fit(X_train, y_train)
  visualizer.fit(X, y)

  # end time
  elapsed = round(time.time() - start, 4)  # round to 4 places
  print(f'Finished in [{elapsed}] seconds')

  # Draw visualization
  outpath = get_chart_outpath(f'{chart_name}_learning-curve')
  visualizer.show(outpath=outpath)

  # reset plot
  visualizer.poof(clear_figure=True)

# https://www.scikit-yb.org/en/latest/api/model_selection/validation_curve.html#
def validation_curve(X, y, model, param_name, param_range, chart_name=''):
  visualizer = ValidationCurve(
    model,
    param_name=param_name,
    param_range=param_range,
    scoring='r2',
    cv=5,
    random_state=random_state
  )
  visualizer.fit(X, y)
  # Draw visualization
  outpath = get_chart_outpath(f'{chart_name}_validation-curve')
  visualizer.show(outpath=outpath)

  # reset plot
  visualizer.poof(clear_figure=True)



# Grid search for optimal hyper parameters of models
def gscv():
  # # Decision Tree
  # dt_param_grid = {
  #   'max_depth': [1,2,3,4,5],
  #   'min_samples_split': [2,3,4],
  # }
  # dt_gs = GridSearchCV(
  #   DecisionTreeClassifier(random_state=random_state),
  #   param_grid=dt_param_grid,
  #   n_jobs=5,
  #   cv=5,
  #   verbose=1,
  # )
  # dt_gs.fit(X_train, y_train)
  # print(dt_gs.best_params_)

  # # Boosting
  # b_param_grid = {
  #   'base_estimator__max_depth': [1,2,3,4,5],
  #   'base_estimator__min_samples_split': [2,3,4],
  #   'n_estimators': [10,50,100,200],
  #   'learning_rate': [0.01, 0.1, 1.0],
  #   'algorithm': ['SAMME', 'SAMME.R']
  # }
  # b_gs = GridSearchCV(
  #   AdaBoostClassifier(base_estimator=DecisionTreeClassifier(random_state=random_state), random_state=random_state),
  #   param_grid=b_param_grid,
  #   n_jobs=5,
  #   cv=5,
  #   verbose=1,
  # )
  # b_gs.fit(X_train, y_train)
  # print(b_gs.best_params_)

  # # Neural Network
  # nn_param_grid = {
  #   'hidden_layer_sizes': [(10), (100), (10,1), (10,10), (10,100), (100,1), (100,10), (100,100)],
  #   'solver': ['lbfgs'],
  #   'alpha': [0.0001, 0.001, 0.01],
  #   'max_iter': [4000]
  # }
  # nn_gs = GridSearchCV(
  #   MLPClassifier(random_state=random_state),
  #   param_grid=nn_param_grid,
  #   n_jobs=5,
  #   cv=5,
  #   verbose=1,
  # )
  # nn_gs.fit(X_train, y_train)
  # print(nn_gs.best_params_)

  # # Support Vector Machine - poly
  # svm_poly_param_grid = {
  #   'kernel': ['poly'],
  #   'C': [0.01, 0.1, 1.0, 10, 100],
  #   'degree': [1,2,3,4,5,6,7],
  #   'gamma': ['scale', 'auto']
  # }
  # svm_poly_gs = GridSearchCV(
  #   svm.SVC(random_state=random_state),
  #   param_grid=svm_poly_param_grid,
  #   n_jobs=5,
  #   cv=5,
  #   verbose=1,
  # )
  # svm_poly_gs.fit(X_train, y_train)
  # print(svm_poly_gs.best_params_)

  # # Support Vector Machine - rbf
  # svm_rbf_param_grid = {
  #   'kernel': ['rbf'],
  #   'C': [0.01, 0.1, 1.0, 10, 100],
  #   'gamma': ['scale', 'auto']
  # }
  # svm_rbf_gs = GridSearchCV(
  #   svm.SVC(random_state=random_state),
  #   param_grid=svm_rbf_param_grid,
  #   n_jobs=5,
  #   cv=5,
  #   verbose=1,
  # )
  # svm_rbf_gs.fit(X_train, y_train)
  # print(svm_rbf_gs.best_params_)

  # # k-Nearest Neighbors
  # knn_param_grid = {
  #   'n_neighbors': [1,2,3,4,5,6,7,8,9,10],
  # }
  # knn_gs = GridSearchCV(
  #   KNeighborsClassifier(),
  #   param_grid=knn_param_grid,
  #   n_jobs=5,
  #   cv=5,
  #   verbose=1,
  # )
  # knn_gs.fit(X_train, y_train)
  # print(knn_gs.best_params_)

  pass


# Models

# Decision Tree
# https://scikit-learn.org/stable/modules/generated/sklearn.tree.DecisionTreeClassifier.html
def dt_base():
  # max_depth=1
  max_depth=None

  model = DecisionTreeClassifier(
    criterion="gini",
    splitter="best",
    max_depth=max_depth,
    max_features=None,
    random_state=random_state
  )

  return model

def dt_tuned():
  '''
  GridSearchCV best params:
  {'max_depth': 2, 'min_samples_split': 2}
  '''
  max_depth=2
  min_samples_split=2

  model = DecisionTreeClassifier(
    criterion="gini",
    splitter="best",
    max_depth=max_depth,
    min_samples_split=min_samples_split,
    max_features=None,
    random_state=random_state
  )

  return model

# Boosting
# https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.AdaBoostClassifier.html
def boosting_base():
  base_estimator = dt_base()

  model = AdaBoostClassifier(
    base_estimator=base_estimator,
    n_estimators=100,
    learning_rate=1.0,
    algorithm='SAMME.R',
    random_state=random_state
  )

  return model

def boosting_tuned():
  '''
  GridSearchCV best params:
  {'algorithm': 'SAMME', 'base_estimator__max_depth': 2, 'base_estimator__min_samples_split': 2, 'learning_rate': 0.1, 'n_estimators': 50}
  '''
  base_estimator = dt_tuned()

  model = AdaBoostClassifier(
    base_estimator=base_estimator,
    n_estimators=50,
    learning_rate=0.1,
    algorithm='SAMME',
    random_state=random_state
  )

  return model

# Neural Network
# https://scikit-learn.org/stable/modules/generated/sklearn.neural_network.MLPClassifier.html
def nn_base():
  model = MLPClassifier(
    solver='lbfgs',
    alpha=1e-5,
    hidden_layer_sizes=(5, 2),
    max_iter=4000,
    random_state=random_state
  )

  return model

def nn_tuned():
  '''
  GridSearchCV best params:
  {'alpha': 0.0001, 'hidden_layer_sizes': (100, 10), 'max_iter': 4000, 'solver': 'lbfgs'}
  '''
  model = MLPClassifier(
    solver='lbfgs',
    alpha=0.0001,
    hidden_layer_sizes=(100, 10),
    max_iter=4000,
    random_state=random_state
  )

  return model

# Support Vector Machine
# https://scikit-learn.org/stable/modules/generated/sklearn.svm.SVC.html
def svm_poly_base():
  model = svm.SVC(
    kernel='poly',
    degree=3,
    C=1,
    gamma='scale',
    random_state=random_state
  )

  return model

def svm_poly_tuned():
  '''
  GridSearchCV best params:
  {'C': 1.0, 'degree': 2, 'gamma': 'scale', 'kernel': 'poly'}
  '''
  model = svm.SVC(
    kernel='poly',
    degree=2,
    C=1,
    gamma='scale',
    random_state=random_state
  )

  return model

def svm_rbf_base():
  model = svm.SVC(
    kernel='rbf',
    C=1,
    gamma='scale',
    random_state=random_state
  )

  return model

def svm_rbf_tuned():
  '''
  GridSearchCV best params:
  {'C': 10, 'gamma': 'scale', 'kernel': 'rbf'}
  '''
  model = svm.SVC(
    kernel='rbf',
    C=10,
    gamma='scale',
    random_state=random_state
  )

  return model

# k-Nearest Neighbors
# https://scikit-learn.org/stable/modules/generated/sklearn.neighbors.KNeighborsClassifier.html
def knn_base():
  model = KNeighborsClassifier(
    n_neighbors=3,      # TODO: start with k=3
    weights='uniform',
    algorithm='auto',
    leaf_size=30,
    p=2,
    metric='minkowski',
    metric_params=None,
    n_jobs=4
  )

  return model

def knn_tuned():
  '''
  GridSearchCV best params:
  {'n_neighbors': 5}
  '''
  model = KNeighborsClassifier(
    n_neighbors=5,
    weights='uniform',
    algorithm='auto',
    leaf_size=30,
    p=2,
    metric='minkowski',
    metric_params=None,
    n_jobs=4
  )

  return model

def class_prediction_error(model, chart_name=''):
  visualizer = ClassPredictionError(
      model, classes=target_names
  )

  # Fit the training data to the visualizer
  visualizer.fit(X_train, y_train)

  # Evaluate the model on the test data
  visualizer.score(X_test, y_test)

  # Draw visualization
  outpath = get_chart_outpath(f'{chart_name}_class-prediction-error')
  visualizer.show(outpath=outpath)

  # reset plot
  visualizer.poof(clear_figure=True)


if __name__ == '__main__':
  print(f'Executing code for dataset [{dataset_name}]')

  # dataset exploration
  class_balance(y)
  feature_correlation(X, y)

  # model selection
  # LearningCurve and CVScores for each model
  learning_curve(X_train, y_train, dt_base(), chart_name='dt-base')
  learning_curve(X_train, y_train, dt_tuned(), chart_name='dt-tuned')

  learning_curve(X_train, y_train, boosting_base(), chart_name='boosting-base')
  learning_curve(X_train, y_train, boosting_tuned(), chart_name='boosting-tuned')

  learning_curve(X_train, y_train, nn_base(), chart_name='nn-base')
  learning_curve(X_train, y_train, nn_tuned(), chart_name='nn-tuned')

  learning_curve(X_train, y_train, svm_poly_base(), chart_name='svm-poly-base')
  learning_curve(X_train, y_train, svm_poly_tuned(), chart_name='svm-poly-tuned')
  learning_curve(X_train, y_train, svm_rbf_base(), chart_name='svm-rbf-base')
  learning_curve(X_train, y_train, svm_rbf_tuned(), chart_name='svm-rbf-tuned')

  learning_curve(X_train, y_train, knn_base(), chart_name='knn-base')
  learning_curve(X_train, y_train, knn_tuned(), chart_name='knn-tuned')

  # for specific model, use grid search or ValidationCurve to tweak specific hyperparameters
  # gscv()

  # Validation Curves for specific hyper parameters
  validation_curve(X_train, y_train, DecisionTreeClassifier(random_state=random_state), 'max_depth', range(1,10), chart_name='dt_max-depth')
  
  validation_curve(X_train, y_train, AdaBoostClassifier(base_estimator=DecisionTreeClassifier(random_state=random_state), random_state=random_state), 'learning_rate', [0.01, 0.1, 1.0], chart_name='boosting_learning-rate')
  validation_curve(X_train, y_train, AdaBoostClassifier(base_estimator=DecisionTreeClassifier(random_state=random_state), random_state=random_state), 'n_estimators', [10,50,100,200], chart_name='boosting_n-estimators')
  
  validation_curve(X_train, y_train, MLPClassifier(solver='lbfgs', max_iter=4000, random_state=random_state), 'alpha', [0.0001, 0.001, 0.01], chart_name='nn_alpha')
  
  validation_curve(X_train, y_train, svm.SVC(kernel='poly', random_state=random_state), 'degree', range(1,7), chart_name='svm-poly_degree')
  validation_curve(X_train, y_train, svm.SVC(kernel='rbf', random_state=random_state), 'C', [0.01, 0.1, 1.0, 10, 100], chart_name='svm-rbf_C')
  
  validation_curve(X_train, y_train, KNeighborsClassifier(), 'n_neighbors', range(1,10), chart_name='knn_n-neighbors')

  # Class Prediction Error
  class_prediction_error(dt_tuned(), chart_name='dt')
  class_prediction_error(boosting_tuned(), chart_name='boosting')
  class_prediction_error(nn_tuned(), chart_name='nn')
  class_prediction_error(svm_poly_tuned(), chart_name='svm-poly')
  class_prediction_error(svm_rbf_tuned(), chart_name='svm-rbf')
  class_prediction_error(knn_tuned(), chart_name='knn')

  # done
  print(f'Completed executing code for dataset [{dataset_name}]')


'''
===
Typical Output:
===

Executing code for dataset [iris]
running class_balance
running feature_correlation
running learning_curve [dt-base]
Finished in [0.7689] seconds
running learning_curve [dt-tuned]
Finished in [0.0234] seconds
running learning_curve [boosting-base]
Finished in [0.0366] seconds
running learning_curve [boosting-tuned]
Finished in [0.2084] seconds
running learning_curve [nn-base]
Finished in [0.0429] seconds
running learning_curve [nn-tuned]
/mnt/c/Dropbox/education/Georgia Tech/CS 7641 - Machine Learning/cs7641-project1/venv/lib/python3.6/site-packages/sklearn/neural_network/_multilayer_perceptron.py:471: ConvergenceWarning: lbfgs failed to converge (status=2):
ABNORMAL_TERMINATION_IN_LNSRCH.

Increase the number of iterations (max_iter) or scale the data as shown in:
    https://scikit-learn.org/stable/modules/preprocessing.html
  self.n_iter_ = _check_optimize_result("lbfgs", opt_res, self.max_iter)
Finished in [0.3911] seconds
running learning_curve [svm-poly-base]
Finished in [0.0269] seconds
running learning_curve [svm-poly-tuned]
Finished in [0.0231] seconds
running learning_curve [svm-rbf-base]
Finished in [0.024] seconds
running learning_curve [svm-rbf-tuned]
Finished in [0.0233] seconds
running learning_curve [knn-base]
Finished in [1.0692] seconds
running learning_curve [knn-tuned]
Finished in [1.0625] seconds
Completed executing code for dataset [iris]
'''
