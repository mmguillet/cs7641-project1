# Machine Learning - Project 1
Matt Guillet
Code: https://bitbucket.org/mmguillet/cs7641-project1/src/master/

## Install and Run
```
# clone repo
git clone git@bitbucket.org:mmguillet/cs7641-project1.git

# go into project directory
cd cs7641-project1

# install python virtual environment
sudo apt install python3-venv

# setup new python virtual environment
python3 -m venv venv

# activate virtual environment
source venv/bin/activate

# update python module installer modules
pip install -U setuptools pip

# install python modules
pip install -r requirements.txt

# Run everything and generate charts in the /charts/iris and /charts/breast-cancer directories
python3 iris.py && python3 breast-cancer.py
```

**Note: Grid search code is commmented out in both python files by default, uncomment to reproduce hyper parameter results.
**Note: /charts/fit-times.png was manually created by recording the fit times output during script execution.

## Cite Sources
* https://scikit-learn.org
* https://www.scikit-yb.org
* https://stackoverflow.com
