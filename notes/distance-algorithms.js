// https://classroom.udacity.com/courses/ud262/lessons/666010252/concepts/6727188270923
// Lesson 5: SL 4 - Instance Based Learning
// Video 7 - Quiz: K NN Bias (The K NN algorithm does not do well trying to guess a function like "y = (x1^2) + x2")

// crow flies distance?
function euclidian(a1, a2) {
  // error
  if (a1.length !== a2.length) {
    console.log('lengths of arrays of points must be equal')
    return false;
  }

  // sum all (pn - qn)^2
  let reducer = (sum, p, index, arr) => {
    let q = a2[index];
    return sum + ((p - q) ** 2)
  }

  // must pass starting sum of 0 for accumulator
  let sum = a1.reduce(reducer, 0)
  let distance = Math.sqrt(sum)
  console.log({distance})
}

euclidian([1,6], [4,2])
euclidian([2,4], [4,2])
euclidian([3,7], [4,2])
euclidian([6,8], [4,2])
euclidian([7,1], [4,2])
euclidian([8,4], [4,2])

// taxi cab distance
function manhattan(a1, a2) {
  // error
  if (a1.length !== a2.length) {
    console.log('lengths of arrays of points must be equal')
    return false;
  }

  // sum all abs(pn - qn)
  let reducer = (sum, p, index, arr) => {
    let q = a2[index];
    return sum + Math.abs(p - q)
  }

  // must pass starting sum of 0 for accumulator
  let distance = a1.reduce(reducer, 0)
  console.log({distance})
}

manhattan([1,6], [4,2])
manhattan([2,4], [4,2])
manhattan([3,7], [4,2])
manhattan([6,8], [4,2])
manhattan([7,1], [4,2])
manhattan([8,4], [4,2])
