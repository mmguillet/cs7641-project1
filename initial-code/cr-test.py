from sklearn.model_selection import TimeSeriesSplit
from sklearn.naive_bayes import GaussianNB

from yellowbrick.classifier import ClassificationReport
from yellowbrick.datasets import load_occupancy

if __name__ == '__main__':
  # Load the classification dataset
  X, y = load_occupancy()

  # Specify the target classes
  classes = ["unoccupied", "occupied"]

  # Create the training and test data
  tscv = TimeSeriesSplit()
  for train_index, test_index in tscv.split(X):
      X_train, X_test = X.iloc[train_index], X.iloc[test_index]
      y_train, y_test = y.iloc[train_index], y.iloc[test_index]

  # Instantiate the classification model and visualizer
  model = GaussianNB()
  visualizer = ClassificationReport(model, classes=classes, support=True)

  visualizer.fit(X_train, y_train)                                          # Fit the visualizer and the model
  visualizer.score(X_test, y_test)                                          # Evaluate the model on the test data
  visualizer.show(outpath='charts/occupancy_dt_classification-report.png')  # Finalize and show the figure

