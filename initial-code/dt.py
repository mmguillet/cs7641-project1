# decision tree
# https://scikit-learn.org/stable/modules/tree.html#tree
# iris tree plot example
# https://scikit-learn.org/stable/auto_examples/tree/plot_iris_dtc.html#sphx-glr-auto-examples-tree-plot-iris-dtc-py
import time

# https://machinelearningmastery.com/how-to-fix-futurewarning-messages-in-scikit-learn/
# import warnings filter
from warnings import simplefilter
# ignore all "future warnings"
simplefilter(action='ignore', category=FutureWarning)

import numpy as np
import matplotlib.pyplot as plt

from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.model_selection import learning_curve
from sklearn.model_selection import StratifiedKFold
from sklearn import tree
from sklearn.tree import DecisionTreeClassifier

from yellowbrick.model_selection import LearningCurve
from yellowbrick.classifier import ClassPredictionError

# TODO: find a dataset with more than 3 classes, but low features (5-10) that might make the decision tree look more interesting
# TODO: or don't, just do binary classification because people say it's easier to work with

# TODO: be careful with passing categorical (text) data into decision tree https://piazza.com/class/kdx36x23bcer4?cid=49


# TODO: use yellowbrick https://www.scikit-yb.org/en/latest/api/classifier/class_prediction_error.html 


# TODO: plot learning curve (MCC (Model Complexity Curve)) https://www.scikit-yb.org/en/latest/api/model_selection/learning_curve.html?highlight=learning#yellowbrick.model_selection.learning_curve.learning_curve

# TODO: watch office hours #3 https://scikit-learn.org/stable/modules/learning_curve.html - has iris example learning curve?

# yellowbrick test
def yb_iris():
  print('yb_iris()')

  # using iris dataset
  X, y = load_iris(return_X_y=True)

  # https://archive.ics.uci.edu/ml/datasets/iris
  classes = ['Iris Setosa', 'Iris Versicolour', 'Iris Virginica']

  # TODO: split dataset into train and test
  # https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.train_test_split.html
  # X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=42, shuffle=True)

  # Perform training/test split
  # .33 test size mis-classifies 2 Iris Virginica as an Iris Versicolour
  X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=55)

  # Instantiate the classification model and visualizer
  visualizer = ClassPredictionError(
      tree.DecisionTreeClassifier(criterion="gini", splitter="best", max_features=None, max_depth=None, min_samples_split=2, min_samples_leaf=1), classes=classes
  )

  # Fit the training data to the visualizer
  visualizer.fit(X_train, y_train)

  # Evaluate the model on the test data
  visualizer.score(X_test, y_test)

  # Draw visualization
  visualizer.show(outpath='charts/iris_dt_test33.png')




  # .2 test size mis-classifies 1 Iris Virginica as an Iris Versicolour
  X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=55)

  # Instantiate the classification model and visualizer
  visualizer = ClassPredictionError(
      tree.DecisionTreeClassifier(criterion="gini", splitter="best", max_features=None), classes=classes
  )

  # Fit the training data to the visualizer
  visualizer.fit(X_train, y_train)

  # Evaluate the model on the test data
  visualizer.score(X_test, y_test)

  # Draw visualization
  visualizer.show(outpath='charts/iris_dt_test20.png')


# sklearn test
def test():



  # print(X)

  # print('len(X)')
  # print(len(X))

  # create tree and train
  # https://scikit-learn.org/0.15/modules/generated/sklearn.tree.DecisionTreeClassifier.html
  clf = tree.DecisionTreeClassifier(criterion="gini", splitter="best", max_features=None)
  # clf = clf.fit(X, y)
  clf = clf.fit(X_train, y_train)

  # plot tree
  plt.suptitle("Decision surface of a decision tree using paired features")
  plt.legend(loc='lower right', borderpad=0, handletextpad=0)
  plt.axis("tight")

  plt.figure()
  tree.plot_tree(clf)
  # clf = DecisionTreeClassifier().fit(iris.data, iris.target)
  # plot_tree(clf, filled=True)
  plt.savefig("iris-dt.png")


# TODO: implement pruning https://piazza.com/class/kdx36x23bcer4?cid=132
# https://stackoverflow.com/questions/49428469/pruning-decision-trees
# https://piazza.com/class/kdx36x23bcer4?cid=90

# TODO: tune hyperparams https://piazza.com/class/kdx36x23bcer4?cid=136

# TODO: use cross_validate() method with param scoring=[list,of,scorers]


def learning_curve(X, y):
  '''
  This method generates a Learning Curve chart for a given model.
  It runs in ~0.9 seconds
    Finished in [0.8959] seconds
    Finished in [0.9006] seconds
  '''
  # start time
  start = time.time()
  print('learning_curve()')
  classes = ['Iris Setosa', 'Iris Versicolour', 'Iris Virginica']


  # Perform training/test split
  # .33 test size mis-classifies 2 Iris Virginica as an Iris Versicolour
  # X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=55)

  # max_depth=1
  max_depth=None

  model = DecisionTreeClassifier(
    criterion="gini",
    splitter="best",
    max_depth=max_depth,
    max_features=None,
    random_state=55
  )

  cv = 5
  sizes = np.linspace(0.1, 1.0, 5)
  # sizes = np.linspace(0.1, 1.0, 10)
  # sizes = [30, 60, 90, 120]

  visualizer = LearningCurve(
    model, cv=cv,
    scoring='f1_weighted',
    train_sizes=sizes,
    n_jobs=5,
    random_state=55
  )
  # visualizer = LearningCurve(model, classes=classes)


  # Fit the training data to the visualizer
  # visualizer.fit(X_train, y_train)
  visualizer.fit(X, y)

  # Draw visualization
  visualizer.show(outpath=f'charts/iris_dt_maxd-{max_depth}_learning-curve.png')

  # reset plot
  visualizer.poof(clear_figure=True)

  # end time
  elapsed = round(time.time() - start, 4)
  print(f'Finished in [{elapsed}] seconds')


if __name__ == '__main__':
  # start time
  start = time.time()
  print('decision tree')
  

  # test()
  yb_iris()
  

  # using iris dataset
  X, y = load_iris(return_X_y=True)

  # https://archive.ics.uci.edu/ml/datasets/iris
  classes = ['Iris Setosa', 'Iris Versicolour', 'Iris Virginica']

  # TODO: split dataset into train and test
  # https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.train_test_split.html
  # X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=42, shuffle=True)



  # learning_curve(X, y)

  
