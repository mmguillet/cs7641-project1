# https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.GridSearchCV.html

import time

# https://machinelearningmastery.com/how-to-fix-futurewarning-messages-in-scikit-learn/
# import warnings filter
from warnings import simplefilter
# ignore all "future warnings"
simplefilter(action='ignore', category=FutureWarning)

import numpy as np

from sklearn.datasets import load_iris
from sklearn.neural_network import MLPClassifier

from yellowbrick.model_selection import LearningCurve

def nn(X, y, random_state=55):
  model = MLPClassifier(
    solver='lbfgs',
    alpha=1e-5,
    hidden_layer_sizes=(5, 2),
    random_state=random_state
  )

  cv = 5
  sizes = np.linspace(0.1, 1.0, 10)

  visualizer = LearningCurve(model, cv=cv, scoring='f1_weighted', train_sizes=sizes, n_jobs=4, random_state=random_state)

  # Fit the training data to the visualizer
  visualizer.fit(X, y)

  # Draw visualization
  visualizer.show(outpath='charts/iris_nn_learning-curve.png')

  # reset plot
  visualizer.poof(clear_figure=True)



if __name__ == '__main__':
  print('neural network')

  # using iris dataset
  X, y = load_iris(return_X_y=True)

  nn(X, y, random_state=55)
