# TODO: normalize dataset to speed up training

# TODO: efficiency https://piazza.com/class/kdx36x23bcer4?cid=49

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.datasets import load_iris
from sklearn.neighbors import KNeighborsClassifier
from sklearn import svm

from yellowbrick.contrib.classifier import DecisionViz
from yellowbrick.model_selection import CVScores

# https://www.scikit-yb.org/en/latest/api/contrib/boundaries.html
def yb_svm():
  # using iris dataset
  X, y = load_iris(return_X_y=True)
  
  # TODO: split data
  # X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.66, random_state=55)
  X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=55)
  
  # # ex1
  # # X = StandardScaler().fit_transform(X)
  # X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=.4, random_state=55)

  # viz = DecisionViz(
  #     KNeighborsClassifier(3), title="Nearest Neighbors",
  #     features=['Feature One', 'Feature Two', 'Feature 3', 'Feature 4'], classes=['A', 'B', 'C']
  # )
  # viz.fit(X_train, y_train)
  # viz.draw(X_test, y_test)
  # visualizer.show(outpath='charts/iris_svm_ex1.png')

  # ex2
  # kernel{‘linear’, ‘poly’, ‘rbf’, ‘sigmoid’, ‘precomputed’}, default=’rbf’
  # clf = svm.SVC(kernel='linear', C=1)
  model = svm.SVC(kernel='poly', degree=3, C=1)
  visualizer = CVScores(model=model, cv=5, scoring='f1_macro')
  # TODO: do not fit this with test data? use all data?
  visualizer.fit(X_train, y_train)
  # visualizer.score(X_test, y_test)
  visualizer.show(outpath='charts/iris_svm_linear.png')

if __name__ == '__main__':
  print('support vector machine')
  yb_svm()
