# https://piazza.com/class/kdx36x23bcer4?cid=140
import time

# https://machinelearningmastery.com/how-to-fix-futurewarning-messages-in-scikit-learn/
# import warnings filter
from warnings import simplefilter
# ignore all "future warnings"
simplefilter(action='ignore', category=FutureWarning)

import numpy as np

from sklearn.datasets import load_iris
from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier

from yellowbrick.model_selection import LearningCurve

def boosting(X, y, random_state=55):
  '''
  https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.AdaBoostClassifier.html
  '''
  # X, y = make_classification(n_samples=1000, n_features=4,
  #                            n_informative=2, n_redundant=0,
  #                            random_state=0, shuffle=False)

  base_estimator = DecisionTreeClassifier(random_state=random_state)

  model = AdaBoostClassifier(base_estimator=base_estimator, n_estimators=100, learning_rate=1.0, algorithm='SAMME.R', random_state=random_state)

  cv = 5
  sizes = np.linspace(0.1, 1.0, 10)

  visualizer = LearningCurve(model, cv=cv, scoring='f1_weighted', train_sizes=sizes, n_jobs=4, random_state=55)

  # Fit the training data to the visualizer
  visualizer.fit(X, y)

  # Draw visualization
  visualizer.show(outpath='charts/iris_boosting_learning-curve.png')

  # reset plot
  visualizer.poof(clear_figure=True)


if __name__ == '__main__':
  print('boosting')

  # using iris dataset
  X, y = load_iris(return_X_y=True)


  boosting(X, y)
