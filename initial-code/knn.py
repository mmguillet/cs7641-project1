import time

# https://machinelearningmastery.com/how-to-fix-futurewarning-messages-in-scikit-learn/
# import warnings filter
from warnings import simplefilter
# ignore all "future warnings"
simplefilter(action='ignore', category=FutureWarning)

import numpy as np

from sklearn.datasets import load_iris
from sklearn.neighbors import KNeighborsClassifier

from yellowbrick.model_selection import LearningCurve

def knn(X, y, random_state=55, n_neighbors=5):
  # n_neighbors=n_neighbors,
  # weights=weights,
  # algorithm=algorithm,
  # leaf_size=leaf_size,
  # p=p,
  # metric=metric,
  # metric_params=metric_params,
  # n_jobs=n_jobs,
  
  # https://scikit-learn.org/stable/modules/generated/sklearn.neighbors.KNeighborsClassifier.html
  model = KNeighborsClassifier(
    n_neighbors=n_neighbors,      # TODO: use different values here for k
    weights='uniform',
    algorithm='auto',
    leaf_size=30,
    p=2,
    metric='minkowski',
    metric_params=None,
    n_jobs=4,
  )

  cv = 5
  sizes = np.linspace(0.1, 1.0, 10)

  visualizer = LearningCurve(model, cv=cv, scoring='f1_weighted', train_sizes=sizes, n_jobs=4, random_state=55)

  # Fit the training data to the visualizer
  visualizer.fit(X, y)

  # Draw visualization
  visualizer.show(outpath=f'charts/iris_knn_{n_neighbors}_learning-curve.png')

  # reset plot
  visualizer.poof(clear_figure=True)
  


if __name__ == '__main__':
  print('k nearest neighbor')

  # using iris dataset
  X, y = load_iris(return_X_y=True)

  knn(X, y, n_neighbors=2)
  knn(X, y, n_neighbors=5)
  knn(X, y, n_neighbors=10)
